from django.urls import path
from course.views import get_course, add_comment, get_comments, get_categories, CourseListView, get_stripe_pub_key

urlpatterns = [
    path('', CourseListView.as_view()),
    path('get_categories/', get_categories),
    path('<slug:slug>/', get_course),
    path('<slug:course_slug>/<slug:lesson_slug>', add_comment),
    path('<slug:course_slug>/<slug:lesson_slug>/get-comments', get_comments),
]