# Generated by Django 4.1.5 on 2023-01-21 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0007_rename_created_buy_comment_created_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to='uploads/'),
        ),
    ]
