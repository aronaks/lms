# Generated by Django 4.1.5 on 2023-01-21 10:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0006_comment'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='created_buy',
            new_name='created_by',
        ),
    ]
