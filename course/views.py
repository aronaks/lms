import stripe
from django.db.models import Count
from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from stripe.error import SignatureVerificationError

from course.models import Course, Lesson, Comment, Category, Transaction
from course.serializers import CourseListSerializer, CourseDetailSerializer, LessonListSerializer, CommentSerializer, \
    CategorySerializer
from lms import settings


class CourseListPagination(PageNumberPagination):
    page_size = 3


@authentication_classes([])
@permission_classes([])
class CourseListView(generics.ListAPIView):
    serializer_class = CourseListSerializer
    pagination_class = CourseListPagination

    def get_queryset(self):
        category_id = self.request.GET.get('category_id')
        courses = Course.objects.all().annotate(
            course_count=Count('transactions__user')).order_by('-course_count')
        # [:10] - у меня это вкладка все категории - так тчо я решил не ограничивать (не пропустил этот момент)
        if category_id:
            courses = courses.filter(categories__in=[int(category_id)])
        return courses


@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def get_course(request, slug):
    course = Course.objects.get(slug=slug)
    course_serializer = CourseDetailSerializer(course)
    lesson_serializer = LessonListSerializer(course.lessons.all(), many=True)
    data = {
        'course': course_serializer.data,
        'lessons': lesson_serializer.data,
    }
    return Response(data)


@api_view(['GET', 'POST'])
def add_comment(request, course_slug, lesson_slug):
    data = request.data
    name = data.get('name')
    content = data.get('content')
    lesson = Lesson.objects.get(slug=lesson_slug)
    course = Course.objects.get(slug=course_slug)
    comment = Comment.objects.create(
        name=name,
        content=content,
        lesson=lesson,
        course=course,
        created_by=request.user,
    )
    serializer = CommentSerializer(comment)
    return Response(serializer.data)


@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def get_comments(request, course_slug, lesson_slug):
    lesson = Lesson.objects.get(slug=lesson_slug)
    serializer = CommentSerializer(lesson.comments.all(), many=True)
    return Response(serializer.data)


@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def get_categories(request):
    categories = Category.objects.all()
    serializer = CategorySerializer(categories, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_stripe_pub_key(request):
    pub_key = settings.STRIPE_PUB_KEY
    return Response({'pub_key': pub_key})


@method_decorator(csrf_exempt, name='dispatch')
class OrderCompleteHook(generics.GenericAPIView):
    def post(self, request, *args, **kwargs):
        stripe.api_key = settings.STRIPE_SECRET_KEY
        endpoint_secret = settings.STRIPE_ENDPOINT_SECRET
        payload = request.body
        sig_header = request.META['HTTP_STRIPE_SIGNATURE']
        event = None

        try:
            event = stripe.Webhook.construct_event(
                payload, sig_header, endpoint_secret
            )
        except ValueError as e:
            # Invalid payload
            return HttpResponse(status=400)
        except SignatureVerificationError as e:
            # Invalid signature
            return HttpResponse(status=400)

        # Handle the checkout.session.completed event
        if event['type'] == 'checkout.session.completed':
            session_id = event['data']['object']['id']
            order = Course.objects.filter(checkout_session=session_id).first()
            if order:
                order.paid = True
                order.save()
                print("Payment was successful.")

        return HttpResponse(status=200)




