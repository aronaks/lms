from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from tinymce import models as tinymce_models


class AbstractTitle(models.Model):
    title = models.CharField(max_length=255)

    class Meta:
        abstract = True
        ordering = ['id']

    def __str__(self):
        return self.title


class Category(AbstractTitle):
    slug = models.SlugField()
    short_description = models.TextField(blank=True)
    created_at = models.DateField(auto_now_add=True)


class Course(AbstractTitle):
    categories = models.ManyToManyField(Category)
    title = models.CharField(max_length=255)
    slug = models.SlugField()
    short_description = models.TextField(blank=True)
    long_description = tinymce_models.HTMLField(blank=True)
    created_at = models.DateField(auto_now_add=True)
    image = models.ImageField(upload_to='uploads/', blank=True, null=True)
    price = models.FloatField(default=0)
    stripe_api_id = models.CharField(max_length=255, blank=True, null=True)
    stripe_link = models.CharField(max_length=255, blank=True, null=True)

    def get_image(self):
        if self.image:
            return settings.WEBSITE_URL + self.image.url
        return 'http://bulma.io/images/placeholders/1280x960.png'


class Lesson(AbstractTitle):
    course = models.ForeignKey(Course, related_name='lessons', on_delete=models.CASCADE)
    slug = models.SlugField()
    short_description = models.CharField(max_length=255, blank=True, null=True)
    long_description = tinymce_models.HTMLField(blank=True, null=True)
    home_work = tinymce_models.HTMLField(blank=True, null=True)
    youtube_id = models.CharField(max_length=20, blank=True, null=True)


class Comment(models.Model):
    course = models.ForeignKey(Course, related_name='comments', on_delete=models.CASCADE)
    lesson = models.ForeignKey(Lesson, related_name='comments', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, related_name='comments', on_delete=models.CASCADE)


class Transaction(models.Model):
    user = models.ForeignKey(User, related_name='transactions', on_delete=models.CASCADE)
    product = models.ForeignKey(Course, related_name='transactions', on_delete=models.CASCADE)
