from django.contrib import admin
from course.models import Category, Course, Lesson, Comment, Transaction


class LessonAdmin(admin.ModelAdmin):
    list_display = ['title', 'course']
    search_fields = ['id', 'title',]
    list_filter = ['course']


class CommentAdmin(admin.ModelAdmin):
    list_display = ['created_by', 'content']
    search_fields = ['name', 'content']
    list_filter = ['course', 'lesson']


class TransactionAdmin(admin.ModelAdmin):
    list_display = ['user', 'product']
    search_fields = ['user', 'product']
    list_filter = ['user', 'product']


admin.site.register(Category)
admin.site.register(Course)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Lesson, LessonAdmin)
admin.site.register(Transaction, TransactionAdmin)
