from django.urls import path
from activity.views import get_active_courses, track_started, mark_as_done, get_completed_courses


urlpatterns = [
    path('get_active_courses/', get_active_courses),
    path('get_completed_courses/', get_completed_courses),
    path('track_started/<slug:course_slug>/<slug:lesson_slug>/', track_started),
    path('mark_as_done/<slug:course_slug>/<slug:lesson_slug>/', mark_as_done),
]