from django.contrib import admin
from activity.models import Activity, CompletedActivity


class ActivityAdmin(admin.ModelAdmin):
    list_display = ['course', 'created_by', 'created_at', 'status']
    search_fields = ['course', 'created_by', 'created_at', 'status']


class CompletedActivityAdmin(ActivityAdmin):
    list_display = ['course', 'created_by', 'created_at']
    search_fields = ['course', 'created_by', 'created_at']


admin.site.register(Activity, ActivityAdmin)
admin.site.register(CompletedActivity, CompletedActivityAdmin)

