import {createRouter, createWebHistory} from 'vue-router'
import SignUp from '../views/SignUp.vue'
import LogIn from '../views/LogIn.vue'
import Courses from "@/views/Courses.vue"
import Course from "@/views/Course.vue";
import MyAccount from '@/views/dashboard/MyAccount.vue'

const routes = [
    {
        path: '/',
        name: 'Courses',
        component: Courses
    },
    {
        path: '/sign-up',
        name: 'SignUp',
        component: SignUp
    },
    {
        path: '/log-in',
        name: 'LogIn',
        component: LogIn
    },

    {
        path: '/dashboard/my-account',
        name: 'MyAccount',
        component: MyAccount
    },
    {
        path: '/courses/:slug',
        name: 'Course',
        component: Course
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
})

export default router
